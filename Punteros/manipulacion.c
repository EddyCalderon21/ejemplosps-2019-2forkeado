#include <stdio.h>


int main(){

	int a= 458;
	int *p;
	p = &a;

	printf("p: %d\n", *p);

	*p = 600;
	printf("a: %d\n", a);

	unsigned long *q = (unsigned long *)&a;
	*q = 0xFFFFFFFFDEADBEEF;

	printf("%d\n", *p);
	return 0;
}
